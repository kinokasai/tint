from arpeggio import Optional, ZeroOrMore, OneOrMore, Not, And, EOF
from arpeggio import RegExMatch as _

def number(): return _(r'[0-9]+')
def string(): return _(r"\".*?\"")
def id(): return Not(keyword), _(r'[a-zA-Z][a-zA-Z0-9_]*')
def keyword(): return Not('int'), _(r'let|in|end|var|function|while|for|to|do|type|break|if|then|else|nil')
def breakpoint(): return '@break'

def postfix_id(): return id, ZeroOrMore(['.', postfix_id]) # This returns var

#def primary_exp(): return [id, number, string, ('(', exps, ')')]

#def postfix_exp(): return primary_exp, ZeroOrMore([('[', exp, ']'), ('.', postfix_exp), ('(', arg_exp, ')')])

def lvalue(): return id, ZeroOrMore([('.', lvalue), ('[', exp, ']')]) #returns a var !

def rvalue(): return [nil_exp, number, string, ('(', exps, ')'), function_call_exp, lvalue]

def unary_exp(): return ZeroOrMore(['+', '-']), rvalue

def mult_exp(): return unary_exp, ZeroOrMore(['*', '/'], mult_exp)

def add_exp(): return mult_exp, ZeroOrMore(['+', '-'], add_exp)

def cmp_exp(): return add_exp, ZeroOrMore(['<=', '<', '>=', '>'], cmp_exp)

def eq_exp(): return cmp_exp, ZeroOrMore(['=', '<>'], eq_exp)

def and_exp(): return eq_exp, ZeroOrMore('&', and_exp)

def or_exp(): return and_exp, ZeroOrMore('|', or_exp)


def assign_exp(): return lvalue, ':=', exp

def array_exp(): return ty_id, '[', exp, '] of', exp

def break_exp(): return 'break'

def for_exp(): return 'for', id, ':=', exp, 'to', exp, 'do', exp

def function_call_exp(): return id, '(', arg_exp, ')'

def if_then_else_exp(): return 'if', exp, 'then', exp, Optional('else', exp)

def let_in_exp(): return 'let', decs, 'in', exps, 'end'

def nil_exp(): return 'nil'

def rec_exp(): return ty_id, '{', rec_fields ,'}'

def while_exp(): return 'while', exp, 'do', exp

exp = [assign_exp, break_exp, breakpoint, for_exp, if_then_else_exp, let_in_exp, while_exp, array_exp, rec_exp, or_exp]

def exps(): return Optional(exp, ZeroOrMore(';', exp))

def entry_point(): return [exp, decs], EOF


def arg_exp(): return Optional(exp, ZeroOrMore(',', exp))

def rec_fields(): return Optional(id, '=', exp, ZeroOrMore(',', id, '=', exp))


def function_dec(): return 'function', id, '(', ty_fields, ')', Optional(':', ty_id), '=', exp

def import_dec(): return 'import', string

def type_dec(): return 'type', id, '=', ty

def var_dec(): return 'var', id, Optional(':', ty_id), ':=', exp

dec = [breakpoint, function_dec, import_dec, type_dec, var_dec]

def decs(): return ZeroOrMore(dec)


def array_ty(): return 'array of', ty_id

def rec_ty(): return '{', ty_fields, '}'

def ty_fields(): return Optional(id, ':', ty_id, ZeroOrMore(',', ty_fields))

def ty_id(): return id

ty = [array_ty, rec_ty, ty_id]
