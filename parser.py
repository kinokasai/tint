from arpeggio import Optional, ZeroOrMore, OneOrMore, Not, And, EOF
from arpeggio import RegExMatch as _

# Terminals

def number(): return _(r'[0-9]+')
def string(): return _(r"\".*?\"")
def id(): return Not(keyword), _(r'[a-zA-Z][a-zA-Z0-9_]*')
def keyword(): return Not(type), _(r'let|in|end|var|function|while|for|to|do|type|break|if|then|else')
def type(): return _(r'int|string')

literal = [number, string]

# Decs

def import_exp(): return 'import ', string

def var_dec(): return 'var', id, Optional(":", id), ":=", exp

def type_dec_field(): return id, ':', id

def type_dec_fields(): return Optional(type_dec_field, ZeroOrMore(',', type_dec_field))

def type_dec(): return 'type', id, '=', '{', type_dec_fields, '}'

def type_alias(): return 'type', id, '=', id

def function_dec(): return 'function', id, '(', function_params, ')', Optional(':', id), "=", exps

# Exps

def assign_exp(): return lvalue, ":=", exp

def break_exp(): return 'break'

def simple_var(): return Not(keyword), Not(type), id

def lvalue_field(): return id, '.', lvalue

def lvalue(): return id, ZeroOrMore('.', lvalue)

def rvalue(): return lvalue

def record_field(): return id, '=', exp

def record_fields(): return record_field, ZeroOrMore(',', record_field)

def record_init(): return id, '{', record_fields,'}'

def function_call(): return id, '(', func_args, ')'

def function_param(): return id, ":", id

def while_exp(): return 'while', exp, 'do', exps

def for_exp(): return 'for', for_assign_exp, 'to', exps, 'do', exps

def for_assign_exp(): return id, ":=", exp

def factor(): return Not(keyword), [terminals, exp]

def term(): return factor, ZeroOrMore(['*', '/'], factor)

def int_exp(): return term, ZeroOrMore(['+', '-'], term)

def if_then_else_exp(): return 'if', exps, 'then', exps, 'else', exps

def if_then_exp(): return 'if', exps, 'then', exps

def op_exp(): return int_exp, ZeroOrMore(['=', '<>','<', '<=', '>', '>='], int_exp)

def let_in_exp(): return 'let', Optional(decs), 'in', Optional(exps), 'end'

def function_params(): return Optional(function_param, ZeroOrMore(',', function_param))

def func_arg(): return exp
def func_args(): return Optional(func_arg, ZeroOrMore(',', func_arg))

def breakpoint(): return '@break'

def exp(): return [breakpoint, break_exp, record_init, let_in_exp, function_call, \
                   assign_exp, while_exp, for_exp, if_then_else_exp, if_then_exp, op_exp, rvalue, literal]

dec = [breakpoint, var_dec, function_dec, type_dec, type_alias, import_exp]

def exps(): return exp, ZeroOrMore(";", exp)
def decs(): return OneOrMore(dec)

begin_exps = exp, ZeroOrMore(";", exp)

def code(): return [decs, begin_exps], EOF

terminals = [literal, rvalue]
