from cmd import Cmd
import pdb

class TDBShell(Cmd):

    prompt = ' -> '
    def __init__(self, debugger):
        super().__init__()
        self.debugger = debugger
        self.last = None

    def do_bye(self, arg):
        return True

    def do_step(self, arg):
        self.last = 'do_step'
        return True

    def do_continue(self, arg):
        self.last = 'do_continue'
        self.debugger.continue_()
        return True

    def do_next(self, arg):
        self.last = 'do_next'
        return True

    def do_backtrace(self, arg):
        self.debugger.print_backtrace()

    def do_change_var(self, arg):
        tmp = arg.split('"')
        exp = tmp[0].split(' ')
        if len(tmp) == 3:
            if exp[1] == 'string':
                exp[2] = '"' + tmp[1] + '"'
            else:
                exp[2] = tmp[1]
        if len(exp) != 3:
            print("Syntax: <var> <type_name> <value>")
        else:
            var = self.debugger.run.get_var_instance(exp[0])
            value = self.debugger.run.get_type_instance(exp[1]).forge(exp[2])
            var.set_value(value)
            self.debugger.run.add_var_to_scope(var)

    def do_EOF(self, arg):
        if self.last:
            getattr(TDBShell, self.last)(self, arg)
        else:
            print()

    def do_print_locals(self, arg):
        self.debugger.run.scope.print_locals()
        self.last = 'do_print_locals'

if __name__ == '__main__':
    TDBShell().cmdloop()
