from arpeggio import ParserPython
from parser import code
from eval import Evaluator
from io import StringIO
import util
import subprocess
import sys
import glob
import os

class Tester:
    def __init__(self):
        self.parser = ParserPython(code)
        self.evaluator = Evaluator(False)

    def eval(self, test):
        code = self.parser.parse(test)
        try:
            self.evaluator.eval(code[0])
        except Exception as e:
            print(util.format_error(e))

    def load_test(self) -> int:
        file_list = glob.glob('tests/*.tig')
        for file_path in file_list:
            f = open('tests/var_dec.tig', 'r')
            test = f.readline()
            output = f.readline()
            code = self.test(test, output)
            if code == 0:
                print(test)
                print(util.quote(output))
                print(util.quote(self.test_output))
                return code
        return code

    def test(self, test, output) -> int:
        sout = sys.stdout
        sys.stdout = StringIO()
        self.eval(test)
        self.test_output = sys.stdout.getvalue()
        sys.stdout = sout
        return int(output == self.test_output)

if __name__ == '__main__':
    def red(string):
        return string

    util.red = red
    tester = Tester()
    exit((tester.load_test() + 1 ) % 2)
