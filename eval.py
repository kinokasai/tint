from runtime import Runtime
from var.simple_var import SimpleVar
from function import Function, Parameter
from type.record_type import RecordType, Field
from util import quote, get_file_contents
import pdb


class Evaluator:

    def __init__(self, run, parser, debug=False):
        self.run = run
        self.parser = parser
        self.debug = debug
        self.space = ''
        self.ty = None # type: Type
        self.looping = 0
        self.breaking = False
        self.no_break = False
        # Used for rvalues
        self.var = None

    # Terminals

    def eval_number(self, exp):
        if self.debug:
            print('int:', exp)
        int_t = self.run.int()
        self.ty = int_t
        return int_t.forge(str(exp))

    def eval_string(self, exp):
        if self.debug:
            print('string:', exp)
        string_t = self.run.string()
        self.ty = string_t
        return string_t.forge(str(exp))

    def eval_id(self, exp):
        if self.debug:
            print('id:', exp)
        return str(exp)

    # Decs

    def eval_import_exp(self, exp):
        if self.debug:
            print('import_exp:', exp)
        s = self.eval(exp[1])
        code = get_file_contents(s)
        code = self.parser.parse(code)
        self.eval(code[0])

    def eval_var_dec(self, exp):
        if self.debug:
            print('var_dec:', exp)
        id = str(exp[1])

        # if type is given
        if len(exp) == 6:
            ty_id = str(exp[3])
            ty = self.run.get_type_instance(ty_id)
            value = self.eval(exp[5])
        else:
            value = self.eval(exp[3])
            ty = self.ty

        if ty.__class__.__name__ == 'RecordType':
            var = SimpleVar(id, ty, value, record=True)
        else:
            var = SimpleVar(id, ty, value)
        var.add_to_scope(self.run, id)

    def eval_type_alias(self, exp):
        if self.debug:
            print('type_alias:', exp)

        id = self.eval(exp[1])
        ty_id = self.eval(exp[3])
        ty = self.run.get_type_instance(ty_id)
        self.run.add_type_to_scope(ty, id)

    def eval_type_dec_field(self, exp):
        id = self.eval(exp[0])
        ty_id = self.eval(exp[2])
        if ty_id != self.ty_def.name:
            ty = self.run.get_type_instance(ty_id)
        else:
            ty = self.ty_def
        field = Field(id, ty)
        return field

    def eval_type_dec_fields(self, exp):
        l = []
        for i in range(0, len(exp), 2):
            field = self.eval(exp[i])
            l.append(field)
        return l

    def eval_type_dec(self, exp):
        id = self.eval(exp[1])
        ty = RecordType(id)
        self.ty_def = ty
        fields = self.eval(exp[4])
        self.ty_def = None
        ty.add_fields(fields)
        self.run.add_type_to_scope(ty)

    def eval_function_param(self, exp):
        id = str(exp[0])
        ty_id = str(exp[2])
        ty = self.run.get_type_instance(ty_id)
        return Parameter(id, ty)

    def eval_function_params(self, exp):
        l = []
        for i in range(0, len(exp), 2):
            param = self.eval(exp[i])
            l.append(param)
        return l

    def eval_function_dec(self, exp):
        if self.debug:
            print('function_dec', exp)

        # Default indexes if everything's present.
        parms_idx = 3
        ty_idx = 6
        body_idx = 8

        id = str(exp[1])
        parms = None

        # If doesn't have parms
        if len(exp) % 2 == 0:
            parms_idx -= 1
            ty_idx -= 1
            body_idx -= 1
        else:
            parms = self.eval(exp[parms_idx])

        # if type is given
        if len(exp) >= 8:
            ty_id = str(exp[ty_idx])
            ty = self.run.get_type_instance(ty_id)
        else:
            body_idx -= 2
            ty = None

        body = exp[body_idx]
        func = Function(id, ty, body)
        if parms:
            func.add_params(parms)

        self.run.add_func_to_scope(func)

    # Exps

    def eval_break_exp(self, exp):
        if not self.looping or self.no_break:
            raise SystemError("Can't break if there's no loop.")
        self.breaking = True

    def eval_exp(self, exp):
        self.var = None
        return self.eval(exp[0])

    def eval_simple_var(self, exp):
        if self.debug:
            print('simple_var:', exp)
        id = self.eval(exp[0])

        self.var = self.run.get_var_instance(id)

        self.ty = self.var.ty
        return self.var.get_value()

    # This returns a value based on an lvalue id
    def eval_rvalue(self, exp):
        if self.debug:
            print('rvalue:', exp)

        id = self.eval(exp[0])
        if len(exp) == 3:
            id += '.' + self.eval(exp[2])

        var = self.run.get_var_instance(id)
        self.ty = var.ty

        return var.value

    # Returns an id
    def eval_lvalue(self, exp) -> str:
        if self.debug:
            print('lvalue:', exp)
        id = self.eval(exp[0])
        if len(exp) == 3:
            id += '.' + self.eval(exp[2])
        return id

    def eval_record_field(self, exp):
        if self.debug:
            print('record_field:', exp)
        id = self.eval(exp[0])
        value = self.eval(exp[2])
        ty = self.ty
        var = SimpleVar(id, ty, value)
        return var

    def eval_record_fields(self, exp):
        if self.debug:
            print('record_fields:', exp)
        vars = []
        for i in range(0, len(exp), 2):
            vars.append(self.eval(exp[i]))
        return vars

    def eval_record_init(self, exp):
        if self.debug:
            print('record_init:', exp)
        ty_id = self.eval(exp[0])
        ty = self.run.get_type_instance(ty_id)
        vars = self.eval(exp[2])
        self.ty = ty
        return vars

    def eval_assign_exp(self, exp):
        id = self.eval(exp[0])
        value = self.eval(exp[2])
        var = self.run.get_var_instance(id)
        var.set_value(value)
        self.run.add_var_to_scope(var)
        self.ty = self.run.void()

    def eval_for_assign_exp(self, exp):
        id = str(exp[0])
        value = self.eval(exp[2])
        ty = self.run.int()
        var = SimpleVar(id, ty, value, const=True)
        return var

    def eval_while_exp(self, exp):
        self.looping += 1
        value = self.eval(exp[1])
        self.run.int().check_instance(value)
        while value and not self.breaking:
            self.eval(exp[3])
            value = self.eval(exp[1])
        self.breaking = False
        self.looping -= 1
        self.ty = self.run.void()

    def eval_for_exp(self, exp):
        if self.debug:
            print('for_exp:', exp)
        self.looping += 1
        self.no_break = True
        var = self.eval(exp[1])
        bound = self.eval(exp[3])
        self.no_break = False
        if not self.ty == self.run.int():
            raise TypeError("Bound should be of type `int`:", self.ty.name)
        start = var.value
        if start > bound:
            raise ValueError("Start value superior to end bound.")
        self.run.enter_scope()
        self.run.add_var_to_scope(var)

        while var.value <= bound and not self.breaking:
            self.eval(exp[5])
            # Modify directly, we know it's a const SimpleVar
            var.value += 1
        self.breaking = False
        self.looping -= 1
        self.ty = self.run.void()
        self.run.quit_scope()

    def eval_if_then_else_exp(self, exp):
        if self.debug:
            print('if_then_else:', exp)
        cdt = self.eval(exp[1])
        self.run.check_int(self.ty)
        if cdt:
            value = self.eval(exp[3])
        else:
            value = self.eval(exp[5])
        # FIXME : Can't check type of both without executing
        return value

    def eval_if_then_exp(self, exp):
        if self.debug:
            print('if_then:', exp)
        cdt = self.eval(exp[1])
        self.ty = self.run.void()
        if cdt:
            return self.eval(exp[3])

    def eval_let_in_exp(self, exp):
        self.run.enter_scope()
        return_value = None
        i = 1
        if exp[1].rule.rule_name != '':
            dec = self.eval(exp[1])
            i += 1
        i += 1
        if exp[i].rule.rule_name != '':
            return_value = self.eval(exp[i])
        self.run.quit_scope()
        # FIXME: check this
        return return_value

    def eval_func_arg(self, exp):
        arg = self.eval(exp[0])
        return arg

    def eval_func_args(self, exp):
        l = []
        for i in range(0, len(exp), 2):
            arg = self.eval(exp[i])
            l.append(arg)
        return l

    def eval_function_call(self, exp):
        if self.debug:
            print('function_call:', exp)
        args = None
        id = self.eval(exp[0])

        if len(exp) >= 4:
            args = self.eval(exp[2])
        func = self.run.get_func_instance(id)
        self.run.enter_scope()
        if args:
            func.add_args(args, self.run)
        result = func.eval(self.run, self)
        self.run.quit_scope()
        self.ty = func.ty
        return result

    def eval_factor(self, exp):
        if self.debug:
            print('factor:', exp)
        lhs = self.eval(exp[0])
        return lhs

    def eval_term(self, exp):
        if self.debug:
            print('term:', exp)
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs
        self.run.check_int(self.ty)
        rhs = self.eval_term(exp[2:])
        self.run.check_int(self.ty)
        if str(exp[1]) == '*':
            return lhs * rhs
        elif str(exp[2]) == '/':
            return lhs / rhs

    def eval_int_exp(self, exp):
        if self.debug:
            print('int_exp:', exp)
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs
        self.run.check_int(self.ty)
        rhs = self.eval_int_exp(exp[2:])
        self.run.check_int(self.ty)
        if str(exp[1]) == '+':
            return lhs + rhs
        elif str(exp[2]) == '-':
            return lhs - rhs

    def eval_op_exp(self, exp):
        if self.debug:
            print('op_exp', exp)

        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        op = str(exp[1])
        cmp = op in ['<', '>', '<=', '>=']

        lhs = self.eval(exp[0])
        lty = self.ty
        if cmp:
            self.run.check_primitive(lty)
        rhs = self.eval_op_exp(exp[2:])
        rty = self.ty
        if cmp:
            self.run.check_primitive(rty)
        if lty != rty:
            lty.expect_error(rty.name)
        self.ty = self.run.int()

        if cmp and not self.run.is_primitive(rty) and self.run.is_primitive(lty):
                raise TypeError(op, 'does not exist for records.')

        if op == '<':
            return int(lhs < rhs)
        elif op == '>':
            return int(lhs > rhs)
        elif op == '<=':
            return int(lhs <= rhs)
        elif op == '>=':
            return int(lhs >= rhs)
        elif op == '=':
            return int(lhs == rhs)
        elif op == '<>':
            return int(lhs != rhs)


    def eval_exps(self, exp):
        if len(exp) == 1:
            return self.eval(exp[0])
        for i in range(0, len(exp), 2):
            value = self.eval(exp[i])
            if i >= len(exp) - 1:
                return value

    def eval_decs(self, exp):
        for dec in exp:
            self.eval(dec)

    def eval_code(self, exp):
        for i in range(0, len(exp), 2):
            value = self.eval(exp[i])
            if i >= len(exp) - 1:
                return value

    def eval_EOF(self, exp):
        return

    def eval(self, tree):
        # This is dirty, please fix this.
        if self.breaking:
            return
        fun = "eval_" + tree.rule.rule_name
        if self.debug:
            print(fun)
        function = getattr(Evaluator, fun)
        return function(self, tree)
