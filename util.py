def quote(string):
    if (type(string) != string):
        return '`' + str(string) + '`'
    return '`' + string + '`'

def space(string):
    return ' ' + string + ' '

def dquote(string):
    return '"'+ string + '"'

def format_error(e):
    return red(e.__class__.__name__) + format_args(e.args)

def format_args(args, end=' '):
    string = ': '
    for arg in args:
        string += arg
        if arg != args[-1]:
            string += end
    return string

def format_parser_error(args):
    string = red("Parsing error") + ': '
    return string + add_args(args)

def add_args(args):
    string = ''
    for arg in args:
        flag = False
        try:
            len(arg)
            flag = True
        except Exception:
            string += str(arg)

        if flag:
            string += add_args(arg)
    return string

def red(string):
    return "\033[31m" + string + "\033[0m"

def orange(string):
    return '\033[47m' + string + "\033[0m"

def color(string):
    tmp = ''
    for c in string:
        if c not in ['$', 'd', '?', 'e', 'p', 'F', '=', 'b', 'y', 'k', 'C', 'U', '*']:
                tmp += red(c)
        else:
            tmp += c
    return tmp

def uncolor(string):
    tmp = ''
    i = 0
    opening = True
    while i < len(string):
        c = string[i]
        if c == '\033':
            i += 4
            if opening:
                i += 1
            opening = not opening
            continue
        else:
            tmp += c
        i += 1
    return tmp

def get_file_contents(file_name):
    f = open(file_name, 'r')
    code = f.read()
    return code
