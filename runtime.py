from var.simple_var import SimpleVar
from type.type import Type
from type.int import Int
from type.string import String
from type.void import Void
from type.nil_type import NilType
from function import Function
from scope import Scope
from util import quote

class Runtime:
    def __init__(self):

        self.scope = Scope()

        int_t = Int()
        string_t = String()
        void_t = Void()
        self.nil_t = NilType()

        self.int_t = int_t
        self.string_t = string_t
        self.void_t = void_t

        self.scope.add_type(int_t)
        self.scope.add_type(string_t)
        self.scope.add_type(void_t)

        print_f = Function('print', void_t, None)
        print_f.add_parm('str', string_t)

        chr_f = Function('chr', string_t, None)
        chr_f .add_parm('code', int_t)

        concat_f = Function('concat', string_t, None)
        concat_f.add_parm('first', string_t)
        concat_f.add_parm('second', string_t)

        exit_f = Function('exit', void_t, None)
        exit_f.add_parm('status', int_t)

        flush_f = Function('flush', void_t, None)

        getchar_f = Function('getchar', string_t, None)

        not_f = Function('not', int_t, None)
        not_f.add_parm('boolean', int_t)

        ord_f = Function('ord', int_t, None)
        ord_f.add_parm('string', string_t)

        print_err_f = Function('print_err', void_t, None)
        print_err_f.add_parm('string', string_t)

        print_int_f = Function('print_int', void_t, None)
        print_int_f.add_parm('value', int_t)

        size_f = Function('size', int_t, None)
        size_f.add_parm('string', string_t)

        strcmp_f = Function('strcmp', int_t, None)
        strcmp_f.add_parm('a', string_t)
        strcmp_f.add_parm('b', string_t)

        streq_f = Function('streq', int_t, None)
        streq_f.add_parm('a', string_t)
        streq_f.add_parm('b', string_t)

        substring_f = Function('substring', string_t, None)
        substring_f.add_parm('string', string_t)
        substring_f.add_parm('first', int_t)
        substring_f.add_parm('length', int_t)

        self.scope.add_func(print_f)
        self.scope.add_func(chr_f)
        self.scope.add_func(concat_f)
        self.scope.add_func(exit_f)
        self.scope.add_func(flush_f)
        self.scope.add_func(getchar_f)
        self.scope.add_func(not_f)
        self.scope.add_func(ord_f)
        self.scope.add_func(print_err_f)
        self.scope.add_func(print_int_f)
        self.scope.add_func(size_f)
        self.scope.add_func(strcmp_f)
        self.scope.add_func(streq_f)
        self.scope.add_func(substring_f)

    # Runtime adders

    def enter_scope(self):
        return self.scope.enter_scope()

    def quit_scope(self):
        return self.scope.quit_scope()

    def add_var_to_scope(self, var):
        self.scope.add_var(var)

    def add_func_to_scope(self, func):
        self.scope.add_func(func)

    def add_type_to_scope(self, ty, name=None):
        self.scope.add_type(ty, name)

    # Runtime getters

    def get_var_instance(self, var_name):
        return self.scope.get_var_instance(var_name)

    def get_type_instance(self, ty_name):
        return self.scope.get_type_instance(ty_name)

    def get_func_instance(self, func_name):
        return self.scope.get_func_instance(func_name)

    # Runtime builtins-type getters

    def int(self):
        return self.int_t

    def string(self):
        return self.string_t

    def void(self):
        return self.void_t

    def nil_type(self):
        return self.nil_t

    # Type checkers

    def check_int(self, ty):
        if ty != self.int():
            self.int().expect_error(ty.name)

    def check_primitive(self, ty):
        if ty != self.int_t and ty != self.string_t:
            raise TypeError("Expected `string` or `int`, got "
                            + quote(ty.name))

    def is_primitive_type(self, value):
        return value == None or type(value) == int or type(value) == str

    def is_primitive(self, ty):
        try:
            self.check_primitive(ty)
        except TypeError:
            return False
        return True

    def check_record(self, var):
        if var.__class__.__name__ != 'RecVar':
            raise TypeError("Not a record")
