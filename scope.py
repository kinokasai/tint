from var.simple_var import SimpleVar
from var.var import Variable
from type.type import Type
from function import Function
from util import quote
from terminaltables import AsciiTable

class Scope:

    def __init__(self):
        self.scope = []
        self.enter_scope()

    def enter_scope(self):
        new_scope = [{}, {}, {}]
        self.scope.append(new_scope)

    def quit_scope(self):
        self.scope = self.scope[:-1]

    # Scope getters

    # scope = [{}, {}, {}]
    def get_current_scope(self):
        return self.scope[-1]

    def get_scoped_instance(self, index, scope, id : str):
        if index >= 3:
            raise IndexError()
        try:
            return scope[index][id]
        except KeyError:
            return None

    def get_scoped_var_instance(self, scope, id):
        return self.get_instance(0, scope, id)

    def get_scoped_func_instance(self, scope, id):
        return self.get_instance(1, scope, id)

    def get_scoped_type_instance(self, scope, id):
        return self.get_instance(2, scope, id)

    def get_instance(self, index, id):
        for scope in reversed(self.scope):
            obj = self.get_scoped_instance(index, scope, id)
            if obj != None:
                return obj

        name = self.get_class_name(index)
        raise LookupError(name + " " + quote(id) + " not in scope.")

    def get_var_instance(self, id):
        return self.get_instance(0, id)

    def get_func_instance(self, id):
        return self.get_instance(1, id)

    def get_type_instance(self, id):
        return self.get_instance(2, id)

    # Scope adders

    def add(self, index, obj, name=None):
        scope = self.get_current_scope()
        if not name:
            name = obj.name
        scope[index].update({name : obj})

    def add_var(self, var : Variable):
        self.add(0, var)

    def add_func(self, func : Function ):
        self.add(1, func)

    def add_type(self, ty : Type, name=None):
        self.add(2, ty, name)

    # Scope Errors
    def get_class_name(self, index):
        if index == 0:
            return 'Variable'
        elif index == 1:
            return 'Function'
        elif index == 2:
            return 'Type'

    # Debugger functions

    @staticmethod
    def merge_subscopes(a, b):
        scope = a
        for i in range(len(a)):
            for elt in b[i].items():
                scope[i].update({elt[0]: elt[1]})
        return scope

    def reduce_scope(self):
        scope = [{},{},{}]
        for subscope in self.scope:
                scope = Scope.merge_subscopes(scope, subscope)
        return scope

    def get_local_vars(self):
        scope = self.reduce_scope()
        return scope[0]

    def print_locals(self):
        scope = self.reduce_scope()
        data = []
        string = ''
        for local in scope[0].items():
            if '.' not in str(local[1]).split(":")[0]:
                string += str(local[1]) + '\n'
        data.append(["Variables", string])
        string = ''
        for local in scope[1].items():
            string += str(local[1]) + '\n'
        data.append(["Functions", string])
        string = ''
        for local in scope[2].items():
            string += str(local[0]) + ' -> ' + str(local[1]) + '\n'
        data.append(["Types", string])
        table = AsciiTable(data)
        table.inner_footing_row_border = True
        print(table.table)
