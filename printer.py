from indent import *
from util import space, dquote

class Printer:

    def __init__(self, prune=False):
        self.pruned = False
        self.prune = prune

    def print_number(self, exp):
        return str(exp)

    def print_string(self, exp):
        return str(exp)

    def print_id(self, exp):
        return str(exp)

    def print_add_exp(self, exp):
        return self.print_cls(exp)

    def print_and_exp(self, exp):
        return self.print_cls(exp)

    def print_assign_exp(self, exp):
        return self.print(exp[0]) + ' := ' + self.print(exp[2])

    def print_arg_exp(self, exp):
        code = self.print(exp[0])
        if len(exp) > 1:
            code += ', ' + self.print(exp[2:][0])
        return code

    def print_array_exp(self, exp):
        code = str(exp[0]) + '[' + self.print(exp[2]) + '] of '
        return code + self.print(exp[4])

    def print_array_ty(self, exp):
        return 'array of ' + str(exp[1])

    def print_break_exp(self, exp):
        return 'break'

    def print_breakpoint(self, exp):
        return '@break'

    def print_decs(self, exp):
        code = iendl() + self.print(exp[0])
        if len(exp) > 1:
            code += self.print_decs(exp[1:])
        return code

    def print_eq_exp(self, exp):
        return self.print_cls(exp)

    def print_exps(self, exp):
        code = iendl() + self.print(exp[0])
        if len(exp) > 1:
            code += ';' + self.print_exps(exp[2:])
        return code

    def print_function_call_exp(self, exp):
        code = str(exp[0]) + '('
        if len(exp) > 3:
            code += self.print(exp[2])
        return code + ')'

    def print_function_dec(self, exp):
        idx = 5
        code = 'function ' + str(exp[1]) + '('
        if len(exp) % 2 != 0:
            code += self.print(exp[3])
            idx += 1

        code += ')'
        if str(exp[idx - 1]) == ':':
            code += ' : ' + self.print(exp[idx])
            idx += 2
        code += ' = ' + incendl() + self.print(exp[idx]) + decindent()
        return code

    def print_for_exp(self, exp):
        code = 'for ' + self.print_assign_exp(exp[1:4]) + ' to '
        code += self.print(exp[5]) + ' do ' + incendl()
        return code + self.print(exp[7]) + decindent()

    def print_if_then_else_exp(self, exp):
        code = 'if ' + self.print(exp[1])
        code +=' then ' + incendl() + self.print(exp[3])
        if len(exp) > 4:
            code += decendl() + 'else' + incendl() + self.print(exp[5])
        return code + decindent()

    def print_import_dec(self, exp):
        return 'import ' + str(exp[1])

    def print_let_in_exp(self, exp):
        code = 'let' + incindent()
        i = 1
        if exp[i].rule.rule_name != '':
            code += self.print(exp[1])
            i += 1

        code += decendl() + 'in' + incindent()
        i += 1
        if exp[i].rule.rule_name != '':
            code += self.print(exp[i])

        return code + decendl() + 'end'

    def print_lvalue(self, exp):
        code = self.print(exp[0])
        if len(exp) > 1:
            code += str(exp[1]) + self.print(exp[2])
        return code

    def print_mult_exp(self, exp):
        return self.print_cls(exp)

    def print_or_exp(self, exp):
        return self.print_cls(exp)

    def print_postfix_exp(self, exp):
        code = ''
        for e in exp:
            code += self.print(e)
        return code

    def print_primary_exp(self, exp):
        code = str(exp[0])
        if len(exp) > 1:
            code = '(' + incindent() + self.print(exp[1]) + decendl() + ')'
        return code

    def print_rec_exp(self, exp):
        code = self.print(exp[0]) + '{' + self.print(exp[2]) + '}'
        return code

    def print_rec_fields(self, exp):
        code = self.print(exp[0])
        if len(exp) > 1:
            self.print_rec_fields(exp[2:])
        return code

    def print_rec_ty(self, exp):
        return '{'+ self.print(exp[1]) + '}'

    def print_cmp_exp(self, exp):
        return self.print_cls(exp)

    def print_rvalue(self, exp):
        if len(exp) == 1:
            return self.print(exp[0])

        code = '(' + incindent() + self.print(exp[1])
        code += decendl() + ')'
        return code

    def print_ty_id(self, exp):
        return str(exp[0])

    def print_type_dec(self, exp):
        return 'type ' + str(exp[1]) + ' = ' + self.print(exp[3])

    def print_ty_fields(self, exp):
        code = str(exp[0]) + ' : ' + str(exp[2])
        if len(exp) > 3:
            code += ', ' + self.print_ty_fields(exp[2:])
        return code

    def print_unary_exp(self, exp):
        if len(exp) == 1:
            return self.print(exp[0])
        return str(exp[0]) + self.print(exp[1])

    def print_var_dec(self, exp):
        code = 'var ' + str(exp[1])
        idx = 3
        if len(exp) == 6:
            idx += 2
            code += ' : ' + str(exp[3])
        return code + ' := ' + self.print(exp[idx])

    def print_while_exp(self, exp):
        code = 'while ' + self.print(exp[1]) + ' do' + incendl()
        code += self.print(exp[3]) + decindent()
        return code

    def print_cls(self, exp):
        code = self.print(exp[0])
        if len(exp) > 1:
            code += space(self.print(exp[1])) + self.print_cls(exp[2:])
        return code

    def print(self, exp):
        if exp.rule.name.startswith('StrMatch'):
            return str(exp.rule)
        fun = 'print_' + exp.rule.rule_name
        function = getattr(Printer, fun)
        return function(self, exp)
