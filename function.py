from util import quote
from var.simple_var import SimpleVar

class Parameter:
    def __str__(self):
        return self.name + ' : ' + self.ty.name

    def __init__(self, name, ty):
        self.name = name
        self.ty = ty

class Function:
    def __str__(self):
        string = self.name + '('
        for p in self.parms:
            string += str(p)
            if p != self.parms[-1]:
                string += ','
        return string + ')'

    def __init__(self, name, ty, body):
        self.name = name
        self.parms = []
        self.body = body
        self.ty = ty
        self.current = 0

    def add_parm(self, name, ty):
        self.parms.append(Parameter(name, ty))

    def add_param(self, parameter):
        self.parms.append(parameter)

    def add_params(self, params : list):
        for param in params:
            if param.name in [x.name for x in self.parms]:
                raise NameError("Redefinition of param", param.name,
                                'in ->', str(self))
            self.parms.append(param)

    def add_args(self, args : list, run):
        if len(args) != len(self.parms):
            raise LookupError("Wrong number of arguments: expected "\
                                + str(len(self.parms)) + \
                              ", got " + str(len(args)))

        for i in range(0, len(args)):
            self.add_arg(args[i], run, i)

    def add_arg(self, value, run, i=0):
        param = self.parms[i]
        expected_ty = param.ty
        expected_ty.check_instance(value)
        var = SimpleVar(param.name, param.ty, value)
        run.add_var_to_scope(var)

    # type(run) = Runtime
    def eval(self, run, evaluator):
        # This is ugly
        if len(self.parms) > 0:
            var = self.parms[0].name
        if self.name == 'print':
            value = run.get_var_instance(var).value
            print(value, end='')
        elif self.name == 'print_int':
            print(run.get_var_instance(self.parms[0].name).value, end='')
        elif self.name == 'chr':
            code = run.get_var_instance(self.parms[0].name).value
            if code < 0 or code > 255:
                raise RuntimeError('chr: character out of range')
            return chr(code)
        elif self.name == 'concat':
            s1 = run.get_var_instance(var).value
            s2 = run.get_var_instance(self.parms[1].name).value
            return s1 + s2
        elif self.name == 'exit':
            code = run.get_var_instance(var).value
            exit(code)
        elif self.name == 'not':
            i = run.get_var_instance(var).value
            return int(not(i))
        elif self.name == 'ord':
            string = run.get_var_instance(var).value
            if len(string) > 0:
                return ord(string[0])
            else:
                return -1
        elif self.name == 'print_err':
            print(run.get_var_instance(var, file=sys.stderr)).value
        elif self.name == 'size':
            string = run.get_var_instance(var).value
            return len(string)
        elif self.name == 'strcmp':
            s1 = run.get_var_instance(var).value
            s2 = run.get_var_instance(self.parms[1].name).value
            raise NotImplementedError()
        elif self.name == 'streq':
            s1 = run.get_var_instance(var).value
            s2 = run.get_var_instance(self.parms[1].name).value
            return int(s1 == s2)
        elif self.name == 'substring':
            s = run.get_var_instance(var).value
            first = run.get_var_instance(self.parms[1].name).value
            length = run.get_var_instance(self.parms[2].name).value
            return s[first:first + length]
        else:
            value = evaluator.eval(self.body)
            if self.ty == None:
                self.ty = evaluator.ty
            if evaluator.ty.name != self.ty.name:
                raise TypeError('Function returned', quote(evaluator.ty.name), 'expected', \
                                quote(self.ty.name))
            return value
