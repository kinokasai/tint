from printer import Printer
from tdb import TDBShell
from math import ceil
from runtime import Runtime
from function import Function, Parameter
from type.record_type import RecordType, Field
from type.array_type import ArrayType
from type.proxy_type import ProxyType
from var.simple_var import SimpleVar
from var.rec_var import RecVar
from var.array_var import ArrayVar
from util import get_file_contents, quote
import pdb

class Debugger():
    def __init__(self, parser, run, debug=False):
        self.parser = parser
        self.debug = debug

        self.back_trace = ['_main']
        self.printer = Printer()
        self.run = run# type: Runtime
        self.shell = TDBShell(self)
        self.ty = None
        self.ty_id = None # Recursive types
        self.looping = 0
        self.breaking = False
        self.no_break = False
        self.chunk = {}
        self.current_var = []

    def eval_number(self, exp):
        int_t = self.run.int()
        self.ty = int_t
        return int_t.forge(str(exp))

    def eval_string(self, exp):
        string_t = self.run.string()
        self.ty = string_t
        return string_t.forge(str(exp))

    def eval_id(self, exp):
        self.ty = 'id'
        return str(exp)

    def eval_add_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        self.run.check_int(self.ty)

        while len(exp) > 1:
            op = str(exp[1])
            rhs = self.eval(exp[2][0])
            self.run.check_int(self.ty)

            if op == '+':
                lhs = lhs + rhs
            elif op == '-':
                lhs = lhs - rhs

            exp = exp[2]

        return lhs

    def eval_and_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        self.run.check_int(self.ty)

        while len(exp) > 1:
            rhs = self.eval(exp[2][0])
            self.run.check_int(self.ty)

            lhs = int(lhs and rhs)

            exp = exp[2]
            lhs = ceil(lhs/(lhs + 1))

        return lhs

    def eval_array_exp(self, exp):
        ty = self.get_type_instance(str(exp[0]))
        size = self.eval(exp[2])
        self.run.check_int(self.ty)
        contents = self.eval(exp[4])
        if ty.ty != self.ty:
            ty.ty.expect_error(self.ty.name)
        vars = []
        for i in range(0, size):
            vid = self.var_id + '[' + str(i) + ']'
            var = self.create_var(vid, ty.ty, contents)
            vars.append(var)
        self.ty = ty
        return vars

    def eval_array_ty(self, exp):
        aty = self.get_type_instance(str(exp[1]))
        id = self.ty_id
        ty = ArrayType(id, aty)
        return ty

    def eval_assign_exp(self, exp):
        var = self.eval(exp[0])
        value = self.eval(exp[2])
        var.set_value(value)
        self.ty = self.run.void()

    def eval_arg_exp(self, l : list, exp) -> list:
        val = self.eval(exp[0])

        l.append(val)
        if len(exp) > 1:
            self.eval_arg_exp(l, exp[2:])

    def eval_break_exp(self, exp):
        self.breaking = True

    def eval_breakpoint(self, exp):
        self.debug = True

    def eval_cmp_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

            self.run.check_primitive(self.ty)

        while len(exp) > 1:

            op = str(exp[1])
            lty = self.ty
            rhs = self.eval(exp[2][0])
            self.run.check_primitive(self.ty)
            rty = self.ty
            if lty != rty:
                lty.expect_error(rty.name)
            self.ty = self.run.int()

            if op == '<':
                lhs = int(lhs < rhs)
            elif op == '<=':
                lhs = int(lhs <= rhs)
            elif op == '>':
                lhs = int(lhs > rhs)
            elif op == '>=':
                lhs = int(lhs >= rhs)

            exp = exp[2]

        return lhs

    def eval_decs(self, exp):
        for dec in exp:
            self.eval(dec)

    def eval_eq_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        while len(exp) > 1:

            op = str(exp[1])
            lty = self.ty
            rhs = self.eval_eq_exp(exp[2][0])
            rty = self.ty

            if rty == self.run.nil_t:
                nonnilty = lty
            elif lty == self.run.nil_t:
                nonnilty = rty

            if rty == self.run.nil_t or lty == self.run.nil_t:
                if lty == self.run.nil_t and rty == self.run.nil_t:
                    raise TypeError("Can't compare nil to nil")
                else:
                    if not self.run.is_primitive_type(nonnilty):
                        raise TypeError("Can't compare nil to" + quote(nonnilty))
                    if op == '=':
                        lhs = 0
                    elif op == '<>':
                        lhs = 1
                    self.ty = self.run.int()
                    exp = exp[2]
                    continue

            if lty != rty:
                lty.expect_error(rty.name)
            self.ty = self.run.int()

            if op == '=':
                lhs = int(lhs == rhs)
            elif op == '<>':
                lhs = int(lhs != rhs)

            exp = exp[2]

        return lhs

    def eval_exps(self, exp):
        if self.debug:
            self.stop(exp[0])
        value = self.eval(exp[0])
        if len(exp) > 1:
            return self.eval_exps(exp[2:])
        else:
            return value

    def eval_function_call_exp(self, exp):
        args = []
        id = self.eval(exp[0])
        func = self.run.get_func_instance(id)
        self.back_trace.append(id)
        self.run.enter_scope()
        if len(exp) > 3:
            self.eval_arg_exp(args, exp[2])
        func.add_args(args, self.run)
        result = func.eval(self.run, self)
        self.run.quit_scope()
        return result

    def eval_function_dec(self, exp):
        self.process_chunk()
        idx = 5
        ty = None
        parms = []
        id = self.eval(exp[1])

        if len(exp) % 2 != 0:
            self.eval_ty_fields(parms, exp[3])
            idx += 1

        if str(exp[idx - 1]) == ':':
            ty = self.eval(exp[idx])
            idx += 2

        body = exp[idx]
        func = Function(id, ty, body)
        if parms:
            func.add_params(parms)

        self.run.add_func_to_scope(func)

    def eval_for_exp(self, exp):
        self.looping += 1
        self.no_break = True
        id = str(exp[1])
        value = self.eval(exp[3])
        ty = self.run.int()
        var = SimpleVar(id, ty, value, const=True)

        bound = self.eval(exp[5])
        self.run.check_int(self.ty)
        self.no_break = False

        start = var.value
        if start > bound:
            raise ValueError("Infinite loop: " + str(bound) + ' < ' + str(start) + '\n'+\
                             self.printer.print(exp))
        self.run.enter_scope()
        self.run.add_var_to_scope(var)

        while var.value <= bound and not self.breaking:
            self.eval(exp[7])
            var.value += 1 # We have to modify it like that
        self.breaking = False
        self.looping -= 1
        self.ty = self.run.void()
        self.run.quit_scope()

    def eval_if_then_else_exp(self, exp):
        cdt = self.eval(exp[1])
        self.run.check_int(self.ty)

        if cdt:
            value = self.eval(exp[3])
            if len(exp) > 4 and value == None:
                raise TypeError("Can't return nil from if then else")
            return value
        elif len(exp) > 4:
            value = self.eval(exp[5])
            if value == None:
                raise TypeError("Can't return nil from if then else")
            return value

    def eval_import_dec(self, exp):
        s = self.eval(exp[1])
        code = get_file_contents(s)
        code = self.parser.parse(code)
        self.eval(code[0])

    def eval_let_in_exp(self, exp):
        self.run.enter_scope()
        value = None
        i = 1
        if exp[i].rule.rule_name != '':
            self.eval(exp[i])
            i += 1
        self.process_chunk()
        i += 1
        if exp[i].rule.rule_name != '':
            value = self.eval(exp[i])
        self.run.quit_scope()
        return value

    def eval_lvalue(self, exp):
        id = str(exp[0])
        self.ty = 'id'
        if self.current_var:
            var = self.current_var.get_value(id)
        else:
            var = self.run.get_var_instance(id)

        if len(exp) == 1:
            return var

        op = str(exp[1])

        if op == '.':
            self.current_var = var
            return self.eval_lvalue(exp[2:][0])
        elif op == '[':
            index = self.eval(exp[2])
            self.run.check_int(self.ty)
            self.ty = 'id'
            var = var.get_value(index)
            if len(exp) > 4:
                self.current_var = var
                var = self.eval_lvalue(exp[5:][0])
            return var

    def eval_mult_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        self.run.check_int(self.ty)

        while len(exp) > 1:
            op = str(exp[1])
            rhs = self.eval(exp[2][0])
            self.run.check_int(self.ty)

            if op == '*':
                lhs = lhs * rhs
            elif op == '/':
                lhs = int(lhs - rhs)

            exp = exp[2]

        return lhs

    def eval_nil_exp(self, exp):
        self.ty = self.run.nil_t
        return None

    def eval_or_exp(self, exp):
        lhs = self.eval(exp[0])
        if len(exp) == 1:
            return lhs

        self.run.check_int(self.ty)

        while len(exp) > 1:
            rhs = self.eval(exp[2][0])
            self.run.check_int(self.ty)

            lhs = int(lhs or rhs)

            exp = exp[2]
            lhs = ceil(lhs/(lhs + 1))

        return lhs

    def eval_rec_exp(self, exp):
        id = str(exp[0])
        ty = self.get_type_instance(id)
        vars = []
        self.eval_rec_fields(vars, exp[2])
        self.ty = ty
        return vars

    def eval_rec_fields(self, l : list, exp : list):
        id = str(exp[0])
        value = self.eval(exp[2])
        ty = self.ty

        var = self.create_var(id, ty, value)
        l.append(var)
        if len(exp) > 3:
            self.eval_rec_fields(l, exp[4:])

    def eval_rec_ty(self, exp):
        id = self.ty_id
        ty = RecordType(id)
        fields = []
        self.eval_ty_fields(fields, exp[1])
        ty.add_fields(fields)
        return ty

    def eval_rvalue(self, exp):
        if len(exp) == 1:
            value = self.eval(exp[0])

            if self.ty == 'id':
                self.ty = value.ty
                return value.get_value()

            return value
        else:
            if len(exp) == 3:
                return self.eval(exp[1])
            else:
                self.ty = self.run.void()

    def eval_ty_id(self, exp):
        return self.get_type_instance(str(exp[0]))

    def eval_type_dec(self, exp):
        id = str(exp[1])
        self.ty_id = id
        proxy = ProxyType(id, exp[3])
        self.chunk.update({id : proxy})
        #self.run.add_type_to_scope(self.eval(exp[3]), id)

    def eval_ty_fields(self, l, exp : list):
        l.append(self.eval_field(exp[:3]))
        if len(exp) > 3:
            self.eval_ty_fields(l, exp[4:][0])

    def eval_field(self, exp):
        id = str(exp[0])
        ty_id = str(exp[2])
        ty = self.get_type_instance(ty_id)
        field = Field(id, ty)
        return field

    def eval_unary_exp(self, exp):
        if len(exp) == 1:
            return self.eval(exp[0])

        if len(exp) > 1:
            op = str(exp[0])
            val = self.eval_unary_exp(exp[1:])
            self.run.check_int(self.ty)

            if op == '-':
                return -val
            else:
                return val

    def eval_var_dec(self, exp):
        self.process_chunk()
        id = str(exp[1])
        self.var_id = id
        idx = 3
        if len(exp) == 6:
            idx += 2
            ty = self.eval(exp[3])
            value = self.eval(exp[idx])
        else:
            value = self.eval(exp[idx])
            if value == None:
                raise TypeError("Can't infer type from nil")
            ty = self.ty

        var = self.create_var(id, ty, value)
        self.run.add_var_to_scope(var)

    def create_var(self, id, ty, value, size=0):
        if ty.__class__.__name__ == 'RecordType':
            var = RecVar(id, ty, value)
        elif ty.__class__.__name__ == 'ArrayType':
            var = ArrayVar(id, ty, value)
        else:
            var = SimpleVar(id, ty, value)
        return var

    def eval_while_exp(self, exp):
        self.looping += 1
        value = self.eval(exp[1])
        self.run.check_int(self.ty)

        while value and not self.breaking:
            self.eval(exp[3])
            value = self.eval(exp[1])
        self.breaking = False
        self.looping -= 1
        self.ty = self.run.void()

    def eval_cls(self, exp):
        pass

    def eval(self, exp):
        if self.breaking:
            return
        fun = 'eval_' + exp.rule.rule_name
        function = getattr(Debugger, fun)
        v = function(self, exp)
        return v

    def get_type_instance(self, ty_name):
        if ty_name in list(self.chunk.keys()):
            return self.chunk[ty_name]
        else:
            return self.run.get_type_instance(ty_name);

    def unwrap(self, id):
        if self.ty == 'id':
            var = self.run.get_var_instance(id)
            self.ty = var.ty
            return var.get_value()
        else:
            return id

    def process_chunk(self):
        values = list(self.chunk.values())
        ids = list(self.chunk.keys())
        exps = []
        for i in range(0, len(self.chunk)):
            proxy = values[i]
            id = ids[i]
            exps.append(proxy.exp)
            self.chunk.update({id : self.eval(proxy.exp)})

        values = list(self.chunk.values())
        ids = list(self.chunk.keys())

        for i in range(0, len(self.chunk)):
            id = ids[i]
            ty = self.eval(exps[i])
            self.chunk.update({id : ty})
            self.run.add_type_to_scope(ty, id)
        self.chunk = {}

    def print_backtrace(self):
        print("Backtrace:")
        length = 0
        for f in self.back_trace:
            if len(f) > length:
                length = len(f)
        template = '  {0:' + str(length) + '} =  {1:10}'
        for f in self.back_trace:
            print(template.format(f, str(hex(id(f)))))

    def stop(self, exp):
        self.printer.code = ''
        print('->' + self.printer.print(exp))
        self.shell.cmdloop()

    def continue_(self):
        self.debug = False
