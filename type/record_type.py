from type.type import Type

class Field:
    def __str__(self):
        return self.name + ':' + self.ty.__str__()

    def __init__(self, name, ty):
        self.name = name
        self.ty = ty

class RecordType(Type):
    def __str__(self):
        string = '{'
        for f in self.fields:
            string += f.__str__()
            if f != self.fields[-1]:
                string += ','
        return string + '}'

    def __init__(self, name):
        self.name = name
        self.fields = []

    def add_fields(self, fields):
        for field in fields:
            self.fields.append(field)

    def forge(self, string):
        raise NotImplementedError("record_forge")

    def check_instance(self, value):
        if value:
            for var in value:
                var.ty.check_instance(var.value)
