from type.type import Type

class NilType(Type):

    def __init__(self):
        self.name = 'nil'
