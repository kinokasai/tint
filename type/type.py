from util import quote

class Type:
    def __str__(self):
        return self.name

    def __init__(self, name):
        raise NotImplementedError("Initialized abstract class: Type")

    def add_field(self, field):
        raise NotImplementedError("add_field")

    def add_fields(self, fields):
        raise NotImplementedError("add_fields")

    def forge(self, string):
        raise NotImplementedError("forge")

    def check_instance(self, value):
        raise NotImplementedError("check_instance")

    # Errors

    def forge_error(self, string):
        raise TypeError("Can't forge", quote(string), 'into', quote(self.name))

    def expect_error(self, name):
        raise TypeError("Expected type", quote(self.name) + ',' ,"got", quote(name))
