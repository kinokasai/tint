from type.type import Type
from type.reg import *

class Int(Type):
    def __init__(self):
        self.name = 'int'

    def add_fields(self, fields):
        raise NotImplementedError("Can't add field to int type.")

    def forge(self, string):
        if not number.match(string):
            self.forge_error(string)

        return int(string)

    def check_instance(self, value):
        if type(value) != int:
            self.expect_error(value)
