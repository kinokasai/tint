from type.type import Type

class ProxyType(Type):

    def __init__(self, name, exp):
        self.name = name
        self.exp = exp
