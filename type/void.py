from type.type import Type
from type.reg import *

class Void(Type):
    def __init__(self):
        self.name = 'void'

    def add_fields(self, fields):
        raise NotImplementedError("Can't add field to int type.")

    def forge(self, string):
        if string != 'nil':
            self.forge_error(string)

        return None

    def check_instance(self, value):
        if value != None:
            self.expect_error(value)
