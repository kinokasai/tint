from type.type import Type
from type import reg

class String(Type):
    def __init__(self):
        self.name = 'string'

    def add_fields(self, fields):
        raise NotImplementedError("Can't add field to string type.")

    def forge(self, string):
        if not reg.string_re.match(string):
            self.forge_error(string)

        string = string.replace("\\n", "\n")
        return string[1:-1]

    def check_instance(self, value):
        if type(value) != str:
            self.expect_error(value)
