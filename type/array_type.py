from type.type import Type

class ArrayType(Type):
    def __str__(self):
        return ' array of ' + self.ty.__str__()

    def __init__(self, name, ty : Type):
        self.name = name
        self.ty = ty

    def check_instance(self, value):
        for var in value:
            self.ty.check_instance(var.value)
