from var.var import Variable

class Record(Variable):
    def __str__(self):
        return self.name + ':' + self.ty + ' -> ' + self.value

    def __repr__(self):
        return self.name + ':' + self.ty + ' -> ' + self.value

    def __init__(self, name, type, value, const=False):
        # TODO Check type
        self.ty = type
        self.name = name
        self.value = value
        self.const = const

    # Value should be a list
    def set_value(self, value : list):
        self.ty.check_instance(value)
        self.value = value
