class Variable:

    def __init__(self, name, ty, value, const=False):
        ty.check_instance(value)
        self.ty = ty
        self.name = name
        self.value = value
        self.const = const

    def get_value(self, index):
        raise NotImplementedError()

    def set_value(self, value):
        if self.const:
            raise AttributeError("Variable", quote(self.name), "is not modifiable.")
        self.ty.check_instance(value)
        self.value = value

    def set_value(self, value):
        raise NotImplementedError("Variable.set_value")
