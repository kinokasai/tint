from var.var import Variable
from util import quote
import pdb

class SimpleVar(Variable):
    def __str__(self):
        return self.name + ':' + self.ty.name + " -> " + str(self.value)

    def __repr__(self):
        return self.name + ':' + self.ty.name + " -> " + str(self.value)

    def __init__(self, name, ty, value, const=False):
        super().__init__(name, ty, value, const)

    def get_value(self, index=None):
        if index:
            raise TypeError("SimpleVar is not indexed.")
        return self.value

    def set_value(self, value):
        if self.const:
            raise AttributeError("Variable", quote(self.name), "is not modifiable.")
        self.ty.check_instance(value)
        self.value = value

    def add_to_scope(self, run, prefix):
        run.add_var_to_scope(self)
