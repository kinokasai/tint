from var.var import Variable
from util import quote

class RecVar(Variable):

    def __str__(self):
        return self.name + ':' + self.ty.name + " -> " + str(self.value)

    def __repr__(self):
        return self.name + ':' + self.ty.name + " -> " + str(self.value)


    def __init__(self, name, ty, value : list, const=False):
        super().__init__(name, ty, value, const)

    def get_value(self, name=None):
        if type(name) == int:
            raise TypeError("RecVar is not indexed.")
        if not name:
            return self.value
        for var in self.value:
            if var.name == name:
                return var
        raise AttributeError(quote(self.name), 'has no field', quote(name))

    def set_value(self, value : list):
        for var in value:
            self.ty.check_instance(var.value)
        self.value = value
