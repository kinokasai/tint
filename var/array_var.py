from var.var import Variable

class ArrayVar(Variable):

    def __str__(self):
        return self.name + ' ' + self.ty.name + ' of size ' + str(self.size)

    def __repr__(self):
        return self.name + ' ' + self.ty.name + ' of size ' + str(self.size)

    def __init__(self, name, ty, value, size=0):
        super().__init__(name, ty, value)
        self.size = len(value)

    def get_value(self, index : int=-1):
        if type(index) == str:
            raise TypeError("ArrayVar has no fields.")
        if index >= 0:
            if index >= self.size:
                raise AttributeError("Out of bounds error:", str(index),
                                    'on array of size',  str(self.size))
            return self.value[index]
        else:
            return self.value

    def set_value(self, value : list):
        for var in value:
            self.ty.check_instance(var.value)
        self.value = value
