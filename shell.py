from cmd import Cmd
from arpeggio import ParserPython, NoMatch
from parser import code
from runtime import Runtime
from eval import Evaluator
from debugger import Debugger
from util import format_error, red, quote, color
from printer import Printer
from grammar import entry_point
from indent import *
import subprocess, pdb

class TigerShell(Cmd):

    def __init__(self):
        super().__init__()
        self.run = Runtime()
        self.parser = ParserPython(entry_point)
        self.printer = Printer()
        self.debug = False
        self.a = 'let function a(a : int) = print_int(a) function b() = \"lol\" in a(b()) end'
        self.cmds = ['EOF', 'quit', 'interpret', 'debug', 'run', 'tigrou', 'help', 'ls', 'sh', 'pprint', 'prune', 'test', 'test_file', 'print_locals', 'print_func']
        self.tigrou = '''
                                   _.- -.- -._     ..
                              .;;"  .oe$$$eeu.. ,?;UU.
                           ,+'!!  e$$$$$$$R$$$$x ?xd$)
                         ,'  !~ u$$$$F,$$$by,?$"e $F"        _ -
               .,;,.   ,dm  :! $$$$$$d( )$( )?d$F       _ -       _
              !?????X!!!!~!!!X!."?????$$$F'.:::::::   -   .  - ~
              +$$$$$m@`!!   ~? `!kCCU$$$$ ::::::::: g~  ~ -  _
               "$BeCeW$:`~..__~x W$$$$$$$e `:::::'.$F           ~
                 "****"      ,``~~?$:=$$$$$$epppe$F`
                         , '   ,    ?W.""??$$FFF" *.
                       '     .     . ?$$e. ==+* ..$f
                    '     . '      H!.?$$$$$$$$$$$f
                       . '        !!!! ?$$$$$$$$$$
                              . ~!!!!!? ?$$$$$$$f.
                          . '     !!!!~*..****C.-!:..
                   . -  ~!!!:.    ?!!~.$$$$$$$$$$.!!!!:` .
              .- ~!::.    !!!!!.  ?X e$$$$$$$$$$$$.`!!!   .! - _
          . `~!?: ~!!!!!  !!!!!!!!! $$$$$$$$$$$$$$$  !!   !!     !?
         '      !!:!!!!!!!!!!!!!!!`:$$$$$$$$$$$$$$$U ~ :!!!!    !!~ `
      .:::::...  !!!!!!!!~~~~!!!!X $$$$$$$$$$$$$$$$$  !!!!!...:<~`   `
     +   ~~!!!!!!!!~~     ::!!!!!":$$$$$$$$$$$$$$$$$~ *~~!!!!*`     .:!
    ::::::::!!!!          "`~~!!~ *""$$$$$$$$$$$$$$$~       !!!!!!!!!``
   ,"`~~!!!!!!~           :        u$$$$$$$$$$$$$$$$~       !!!!``"   ;
  . .... ~!!!             .   ..+ $$$$$$$$$$$$$$$$$$~      .!!!!!!?:::-
  !!!!!!!!!!!!::::+       !!~~~~~ **""$$$$$$$$$$$$$*    .+!!!!!!!!(  ,
 ;!!!!!!!!!!!!!!!~        !!!::.. ue$$$$$$$$$$$$$$$~ !!!!!!!!!!!!!!!!
 !!!!!!!!!!!``            ~!!!!!! $$$$$$$$$$$$$$$$$     !!!!!!!!!!!!
 !!!!!!!!!!!             .   `~~! $$$$$$$$$$$$$$$$`    :!!!!!!!!!!X`
 ~!!!!!!!!!!             ....::!! $$$$$$$$$$$$$$$$     !!!!!!!!!!~
  `~!!!!!!!!             !!!!!!!!m$$$$$$$$$$$$$$$      !!!!!!!*`
    `*~X!~*              !!!!!!!*:$$$$$$$$$$$$$$~      ``((`
                       :!!::   ` "?I$$$$$$$$$$$`
                       `~!!!!!- e$$$$$$$$$$$$$~
                    '     `*~! $$$$$$$$$$$$$$" .
                 .:!:.     .: e$$$$$$$$$$$$$" ~  `
               '    `~!!!!!!~:$$$$$$$$$$$$* :*    :!       ......
            ::::::..   ~!!!` $$$$$$$$$$$$":!~  :!!` `  .!!!!!!!!!!
           -~!!!!!!!!::..!! 8$$$$$$$$$$*.!!!..!!~ ..- :!!!!!!!~!!!~
         '     "~~!!!!!!!!! #$$$$$$$$*(!!!!!!!!!.+!~ :!!!!!~`- -`!
       ;            `!!!!!!!."*$$**" `````"`!!!!!!! !!!!!~
                 ..  .!!!!!~~`               `!!!!`:!!!`        '
       `.:!!!!!!!!!!!!!!~:                     !!!!!!!~       '
        ~!!!!!!!~~`~!!!!!!:::                  ~!!!!!!      '
                 .:!!!!!!!!!!!!:                `!!!!!_  '
               :::::::!?!!!!!!!!!
            :!!!!!!!!!!!!!!!!!!~
            `~!!!!!!!!!!!!!~~

        '''

    intro = 'Welcome to *Tint*'
    prompt = '>>> '
    file = None

    def do_EOF(self, arg):
        return exit

    def do_test_file(self, arg):
        try:
            parser = ParserPython(entry_point, debug=True)
            parser.parse(self.get_code(arg))
        except NoMatch as e:
            print(red('Parser Error:'), str(e))

    def do_test(self, arg):
        try:
            parser = ParserPython(entry_point, debug=True)
            parser.parse(arg)
        except NoMatch as e:
            print(red('Parser Error:'), str(e))

    def do_quit(self, arg):
        return True

    def do_debug(self, arg):
        self.debug = not self.debug

    def do_interpret(self, arg):
        try:
            code = self.parser.parse(arg)
            evaluator = Debugger(self.parser, self.run, debug=False)
            result = evaluator.eval(code[0])
            evaluator.process_chunk()
            if result != None:
                print(result)
        except NoMatch as e:
            print(red('Parser Error:'), str(e))
        except KeyboardInterrupt:
            print()
        except Exception as e:
            if self.debug:
                raise e
            else:
                print(format_error(e))

    def do_run(self, arg):
            self.do_interpret(self.get_code(arg))

    def do_pprint(self, arg, prune=False):
        try:
            code = self.parser.parse(self.get_code(arg))
            printer = Printer(prune=prune)
            print(printer.print(code[0]))
        except SystemError as e:
            print(e)

    def do_print_locals(self, arg):
        self.run.scope.print_locals()

    def do_print_func(self, arg):
        try:
            func = self.run.get_func_instance(arg)
        except Exception as e:
            print(format_error(e))

        print(str(func), Printer().print(func.body), sep=' ->'+incendl())

    def do_prune(self, arg):
        self.do_print(arg, prune=True)

    def do_ls(self, arg):
        files = self.get_files()
        for f in files:
            print(f)

    def do_sh(self, arg):
        args = arg.split(' ')
        try:
            print(subprocess.check_output(args).decode())
        except BaseException:
            print("Erroneous command")

    def do_tigrou(self, arg):
        '''tigrou [--color]:
    prints a tigrou.'''
        if arg == '--color':
            print(color(self.tigrou))
        else:
            print(self.tigrou)

    def complete_run(self, text, line, begidx, endidx):
        return [i for i in self.get_files() if i.startswith(text)]

    def complete_print(self, text, line, begidx, endidx):
        return [i for i in self.get_files() if i.startswith(text)]

    def complete_prune(self, text, line, begidx, endidx):
        return [i for i in self.get_files() if i.startswith(text)]

    def get_files(self):
        l = subprocess.check_output(['ls']).decode().split('\n')[:-1]
        return l

    def get_code(self, arg):
        if arg == '':
            print('Please specify file.')
        else:
            f = open(arg, 'r')
            code = f.read()
            return code

    def precmd(self, line):
        if line == '':
            return line
        for cmd in self.cmds:
            if line.startswith(cmd):
                return line
        else:
            s = 'interpret ' + line
            return s
